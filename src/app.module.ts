import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_INTERCEPTOR } from '@nestjs/core';

import { LoggerMiddleware } from './shared/middleware/logger.middleware';
import { TransformDataInterceptor } from './shared/interceptors/transform-data.interceptor';

import { RolesModule } from './roles/roles.module';
import { AccountsModule } from './accounts/accounts.module';
import { SessionsModule } from './sessions/sessions.module';
import { UsersModule } from './users/users.module';
import { ProfilesModule } from './profiles/profiles.module';

import { Account } from './accounts/entities/account.entity';
import { Role } from './roles/entities/role.entity';
import { Session } from './sessions/entities/session.entity';
import { Profile } from './profiles/entities/profile.entity';
import { TimeoutInterceptor } from './shared/interceptors/timeout.interceptor';
import { CryptoModule } from './crypto/crypto.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      port: parseInt(process.env.POSTGRES_PORT, 10) || 5432,
      host: process.env.POSTGRES_HOST ?? 'localhost',
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      synchronize: true,
      entities: [Account, Role, Profile, Session],
    }),
    AccountsModule,
    RolesModule,
    SessionsModule,
    UsersModule,
    ProfilesModule,
    CryptoModule,
    AuthModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformDataInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
