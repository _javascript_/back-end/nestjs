import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from 'src/accounts/entities/account.entity';
import { Repository } from 'typeorm';
import { CreateSessionDto } from './dto/create-session.dto';
import { UpdateSessionDto } from './dto/update-session.dto';
import { Session } from './entities/session.entity';

@Injectable()
export class SessionsService {
  constructor(
    @InjectRepository(Session) private sessionRepo: Repository<Session>,
  ) {}
  create(account: Account, createSessionDto: CreateSessionDto) {
    const session = this.sessionRepo.create(createSessionDto);
    session.account = account;
    return this.sessionRepo.save(session);
  }

  findAll() {
    return `This action returns all sessions`;
  }

  findOneById(id: number) {
    return `This action returns a #${id} session`;
  }

  findOneByToken(token: string) {
    return this.sessionRepo.findOneBy({ token });
  }

  update(id: number, updateSessionDto: UpdateSessionDto) {
    return `This action updates a #${id} session`;
  }

  remove(session: Session) {
    return this.sessionRepo.remove(session);
  }
}
