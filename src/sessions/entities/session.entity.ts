import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { Account } from 'src/accounts/entities/account.entity';

@Entity('sessions')
export class Session {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'qwe-rty-iop', description: 'Refresh Token' })
  @Column({ nullable: true })
  token: string;

  @ApiProperty({ example: '127.0.0.1', description: 'IP' })
  @Column({ nullable: true })
  ip: string;

  @ApiProperty({
    type: 'jsonb',
    example: '{"test": "test"}',
    description: 'Meta data',
  })
  @Column({ nullable: false })
  data: string;

  @ManyToOne(() => Account, (account) => account.sessions, {
    onDelete: 'CASCADE',
  })
  account: Account;
}
