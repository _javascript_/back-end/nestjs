import { ApiProperty } from '@nestjs/swagger';

export class CreateSessionDto {
  @ApiProperty({ example: 'qwe-rty-iop', description: 'Refresh Token' })
  token: string;

  @ApiProperty({ example: '127.0.0.1', description: 'IP' })
  ip: string;

  @ApiProperty({
    type: 'jsonb',
    example: '{"test": "test"}',
    description: 'Meta data',
  })
  data: string;
}
