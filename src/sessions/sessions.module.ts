import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Account } from 'src/accounts/entities/account.entity';

import { Session } from './entities/session.entity';
import { SessionsService } from './sessions.service';

@Module({
  imports: [TypeOrmModule.forFeature([Session, Account])],
  providers: [SessionsService],
  exports: [SessionsService],
})
export class SessionsModule {}
