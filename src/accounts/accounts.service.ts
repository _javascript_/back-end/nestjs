import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CryptoService } from 'src/crypto/crypto.service';
import { Profile } from 'src/profiles/entities/profile.entity';
import { Role, RoleValue } from 'src/roles/entities/role.entity';
import { Repository } from 'typeorm';

import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';

import { Account } from './entities/account.entity';

/* 
  TODOS:
  1. Pass Hash
  2. Returning ResAcc
*/

@Injectable()
export class AccountsService {
  private defaultRole = { value: RoleValue.USER };
  private relations = ['roles', 'profile', 'sessions'];
  constructor(
    @InjectRepository(Account) private accountsRepository: Repository<Account>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
    @InjectRepository(Profile) private profilesRepository: Repository<Profile>,
    private cryptoService: CryptoService,
  ) {}

  async create(createAccountDto: CreateAccountDto) {
    const { email, password } = createAccountDto;

    const candidate = await this.accountsRepository.findOneBy({ email });
    if (candidate) {
      const msg = `Пользователь с EMAIL ${email} уже существует.`;
      throw new HttpException(msg, 400);
    }

    const hashedPassword = await this.cryptoService.hash(password);

    const dto = { email, password: hashedPassword };
    const account = this.accountsRepository.create(dto);

    const role = await this.rolesRepository.findOneBy(this.defaultRole);

    const profile = this.profilesRepository.create();

    account.profile = profile;
    account.roles = [role];

    await this.profilesRepository.save(profile);
    await this.accountsRepository.save(account);

    return account;
  }

  findAll() {
    const { relations } = this;
    return this.accountsRepository.find({ relations });
  }

  findOneById(id: number) {
    const options = { where: { id }, relations: this.relations };
    const account = this.accountsRepository.findOne(options);
    if (!account) {
      const msg = 'User with this id does not exist';
      throw new HttpException(msg, HttpStatus.NOT_FOUND);
    }
    return account;
  }

  findOneByEmail(email: string) {
    const options = { where: { email }, relations: this.relations };
    const account = this.accountsRepository.findOne(options);
    if (!account) {
      const msg = 'User with this email does not exist';
      throw new HttpException(msg, HttpStatus.NOT_FOUND);
    }
    return account;
  }

  async update(id: number, updateAccountDto: UpdateAccountDto) {
    return this.accountsRepository.update(id, updateAccountDto);
  }

  async remove(id: number) {
    const account = await this.findOneById(id);
    return this.accountsRepository.remove(account);
  }
}
