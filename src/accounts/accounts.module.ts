import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AccountsService } from './accounts.service';
import { AccountsController } from './accounts.controller';

import { Account } from './entities/account.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Session } from 'src/sessions/entities/session.entity';
import { Profile } from 'src/profiles/entities/profile.entity';
import { CryptoModule } from 'src/crypto/crypto.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Account, Role, Profile, Session]),
    CryptoModule,
    forwardRef(() => AuthModule),
  ],
  controllers: [AccountsController],
  providers: [AccountsService],
  exports: [AccountsService],
})
export class AccountsModule {}
