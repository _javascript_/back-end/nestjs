import { ApiProperty } from '@nestjs/swagger';
import { CreateProfileDto } from 'src/profiles/dto/create-profile.dto';
import { RoleValue } from 'src/roles/entities/role.entity';

import { Account } from '../entities/account.entity';

export class ResponseAccountDto {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  id: number;

  @ApiProperty({ example: 'my-email@gmail.com', description: 'Email' })
  email: string;

  @ApiProperty({ example: ['ADMIN'], description: 'Роли пользователя' })
  roles: RoleValue[];

  @ApiProperty({
    example: CreateProfileDto,
    description: 'Профиль пользователя',
  })
  profile: CreateProfileDto;

  constructor({ email, id, profile, roles }: Account) {
    roles = roles?.length ? roles : [];
    delete profile['id'];
    this.email = email;
    this.id = id;
    this.profile = profile;
    this.roles = roles.map((role) => role.value);
  }

  toObject() {
    return {
      id: this.id,
      email: this.email,
      roles: this.roles,
      profile: this.profile,
    };
  }
}
