import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, Length } from 'class-validator';

export class CreateAccountDto {
  @ApiProperty({ example: 'my-email@gmail.com', description: 'Email' })
  @IsEmail()
  email: string;

  @ApiProperty({ example: 'Qwe123poi@', description: 'Пароль' })
  @Length(3, 64)
  password: string;
}
