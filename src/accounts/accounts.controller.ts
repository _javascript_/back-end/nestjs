import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Roles } from 'src/auth/roles-auth.decorator';
import { RolesGuard } from 'src/auth/roles.guard';
import { RoleValue } from 'src/roles/entities/role.entity';
import { ValidationPipe } from 'src/shared/pipes/validation.pipe';
import { AccountsService } from './accounts.service';
import { CreateAccountDto } from './dto/create-account.dto';
import { ResponseAccountDto } from './dto/response-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { Account } from './entities/account.entity';

@ApiTags('Аккаунты')
@UseGuards(JwtAuthGuard)
@Controller('accounts')
export class AccountsController {
  constructor(private readonly accountsService: AccountsService) {}

  @ApiOperation({ summary: 'Создание аккаунта' })
  @ApiResponse({ status: 201, type: ResponseAccountDto })
  @Post()
  @UsePipes(new ValidationPipe())
  async create(@Body() createAccountDto: CreateAccountDto) {
    const account = await this.accountsService.create(createAccountDto);
    return new ResponseAccountDto(account);
  }

  @ApiOperation({ summary: 'Получиние всех аккаунтов' })
  @ApiResponse({ status: 200, type: [Account] })
  @Get()
  async findAll() {
    const accounts = await this.accountsService.findAll();
    return accounts.map((account) => new ResponseAccountDto(account));
  }

  @ApiOperation({ summary: 'Получиние аккаунта по ID' })
  @ApiResponse({ status: 200, type: Account })
  @Roles(RoleValue.ADMIN)
  @UseGuards(RolesGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.accountsService.findOneById(+id);
  }

  @ApiOperation({ summary: 'Изменение аккаунта по ID' })
  @ApiResponse({ status: 200, type: Account })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAccountDto: UpdateAccountDto) {
    return this.accountsService.update(+id, updateAccountDto);
  }

  @ApiOperation({ summary: 'Удалиние аккаунта по ID' })
  @ApiResponse({ status: 200, type: Account })
  @Delete(':id')
  remove(@Param('id') id: string) {
    console.log('REMOVE', id);
    return this.accountsService.remove(+id);
  }
}
