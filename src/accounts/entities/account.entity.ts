import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  OneToOne,
  OneToMany,
} from 'typeorm';

import { Role, RoleValue } from 'src/roles/entities/role.entity';
import { Session } from 'src/sessions/entities/session.entity';
import { Profile } from 'src/profiles/entities/profile.entity';

@Entity('accounts')
export class Account {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'my-email@gmail.com', description: 'Email' })
  @Column({ unique: true, nullable: false })
  email: string;

  @ApiProperty({ example: 'Qwe123poi@', description: 'Пароль' })
  @Column({ nullable: false })
  password: string;

  @ApiProperty({ example: ['ADMIN'], description: 'Роли пользователя' })
  @ManyToMany(() => Role, (role) => role.accounts)
  @JoinTable()
  roles: Role[];

  @ApiProperty({ example: Profile, description: 'Профиль пользователя' })
  @OneToOne(() => Profile, (profile) => profile.account, {
    onDelete: 'CASCADE',
  })
  profile: Profile;

  // @ApiProperty({ example: Session, description: 'Сессии пользователя' })
  @OneToMany(() => Session, (session) => session.account, {
    onDelete: 'CASCADE',
  })
  sessions: Session[];
}
