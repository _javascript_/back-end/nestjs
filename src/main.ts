import * as fs from 'fs';
import * as cookieParser from 'cookie-parser';

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app.module';
import { HttpExceptionFilter } from './shared/exceptions/http-exception.filter';

// import { Logger, ValidationPipe } from '@nestjs/common';
// import { ValidationPipe } from './shared/pipes/validation.pipe';

const httpsOptions = {
  key: fs.readFileSync('./cert/key.pem'),
  cert: fs.readFileSync('./cert/cert.pem'),
};

async function bootstrap() {
  const PORT = process.env.PORT ?? 5000;
  const app = await NestFactory.create(AppModule, {
    httpsOptions,
    cors: {
      credentials: true,
      origin: 'https://localhost:3000',
    },
  });

  app.setGlobalPrefix('api/v1');
  app.useGlobalFilters(new HttpExceptionFilter());
  app.use(cookieParser());

  // app.useGlobalPipes(new ValidationPipe());

  const config = new DocumentBuilder()
    .setTitle('Практика по BACKEND разработке')
    .setDescription('Документация REST API')
    .setVersion('1.0.0')
    .addTag('LeshDK')
    .build();

  const documnet = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/swagger', app, documnet);

  await app.listen(PORT, () => {
    Logger.log(`Server is running at https://localhost:${PORT}`, 'BootStrap');
  });
}
bootstrap();
