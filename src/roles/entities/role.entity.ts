import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany } from 'typeorm';

import { Account } from 'src/accounts/entities/account.entity';

export enum RoleValue {
  ADMIN = 'ADMIN',
  GUEST = 'GUEST',
  USER = 'USER',
}

@Entity('roles')
export class Role {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'ADMIN', description: 'Название роли' })
  @Column({ unique: true, nullable: false })
  value: RoleValue;

  @ApiProperty({
    example: 'Пользователь с правами на удаление других пользователей',
    description: 'Описание роли',
  })
  @Column({ nullable: false })
  description: string;

  @ManyToMany(() => Account, (account: Account) => account.roles)
  accounts: Account[];
}
