import { ApiProperty } from '@nestjs/swagger';
import { RoleValue } from '../entities/role.entity';

export class CreateRoleDto {
  @ApiProperty({ example: RoleValue.ADMIN, description: 'Название роли' })
  value: RoleValue;

  @ApiProperty({
    example: 'Пользователь с правами на удаление других пользователей',
    description: 'Описание роли',
  })
  description: string;
}
