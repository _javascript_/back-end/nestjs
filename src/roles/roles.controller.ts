import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Role } from './entities/role.entity';

@ApiTags('Роли')
@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @ApiOperation({ summary: 'Создание ролей' })
  @ApiResponse({ status: 201, type: Role })
  @Post()
  create(@Body() createRoleDto: CreateRoleDto) {
    return this.rolesService.create(createRoleDto);
  }

  @ApiOperation({ summary: 'Получиние всех ролей' })
  @ApiResponse({ status: 200, type: [Role] })
  @Get()
  findAll() {
    return this.rolesService.findAll();
  }

  @ApiOperation({ summary: 'Создать роль: Админ, Гость, Пользователь' })
  @ApiResponse({ status: 200, type: [Role] })
  @Get('/default')
  createDefaultRoles() {
    return this.rolesService.createDefaultRoles();
  }

  @ApiOperation({ summary: 'Получиние роли по ID' })
  @ApiResponse({ status: 200, type: [Role] })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.rolesService.findOne(+id);
  }

  @ApiOperation({ summary: 'Изменение роли по ID' })
  @ApiResponse({ status: 200, type: [Role] })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRoleDto: UpdateRoleDto) {
    return this.rolesService.update(+id, updateRoleDto);
  }

  @ApiOperation({ summary: 'Удалиние роли по ID' })
  @ApiResponse({ status: 200, type: Role })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.rolesService.remove(+id);
  }
}
