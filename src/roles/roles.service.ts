import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from 'src/accounts/entities/account.entity';
import { Repository } from 'typeorm';

import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';

import { Role, RoleValue } from './entities/role.entity';

@Injectable()
export class RolesService {
  private default = {
    [RoleValue.ADMIN]: 'Роль для господ',
    [RoleValue.GUEST]: 'Роль для холопов',
    [RoleValue.USER]: 'Роль для работяг',
  } as const;

  constructor(
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}

  create(createRoleDto: CreateRoleDto) {
    const role = this.rolesRepository.create(createRoleDto);
    return this.rolesRepository.save(role);
  }

  findAll() {
    return this.rolesRepository.find();
  }

  findOne(id: number) {
    return this.rolesRepository.findOneBy({ id });
  }

  findOneByValue(value: RoleValue) {
    return this.rolesRepository.findOneBy({ value });
  }

  update(id: number, updateRoleDto: UpdateRoleDto) {
    return this.rolesRepository.update(id, updateRoleDto);
  }

  async remove(id: number) {
    const role = await this.findOne(id);
    return this.rolesRepository.remove(role);
  }

  async createDefaultRoles() {
    const roles = [];
    for (const item in this.default) {
      const description = this.default[item];
      const value = item as RoleValue;
      const candidate = await this.findOneByValue(value);
      if (candidate) continue;
      const role = await this.create({ value, description });
      roles.push(role);
    }
    return roles;
  }

  async addRole(account: Account, value = RoleValue.USER) {
    const role = await this.findOneByValue(value);
    if (!role) return;
    role.accounts = [account];
    await this.rolesRepository.save(role);
    return role;
  }
}
