import { ApiProperty } from '@nestjs/swagger';

import { Account } from 'src/accounts/entities/account.entity';
import { Profile } from 'src/profiles/entities/profile.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Session } from 'src/sessions/entities/session.entity';

export class User {
  @ApiProperty({ example: Account, description: 'Аккаунте пользователя' })
  account: Account;

  @ApiProperty({ example: Profile, description: 'Профиль пользователя' })
  profile: Profile;

  @ApiProperty({ example: [Role], description: 'Роли пользователя' })
  roles: Role[];

  @ApiProperty({ example: [Session], description: 'Сессии пользователя' })
  sessions?: Session[];
}
