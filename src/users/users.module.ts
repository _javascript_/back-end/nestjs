import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Account } from 'src/accounts/entities/account.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Session } from 'src/sessions/entities/session.entity';

import { RolesModule } from 'src/roles/roles.module';
import { SessionsModule } from 'src/sessions/sessions.module';
import { AccountsModule } from 'src/accounts/accounts.module';
import { ProfilesModule } from 'src/profiles/profiles.module';
import { Profile } from 'src/profiles/entities/profile.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Account, Role, Profile, Session]),
    RolesModule,
    SessionsModule,
    AccountsModule,
    ProfilesModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
