import { IntersectionType, PartialType } from '@nestjs/swagger';
import { CreateProfileDto } from 'src/profiles/dto/create-profile.dto';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends IntersectionType(
  PartialType(CreateUserDto),
  PartialType(CreateProfileDto),
) {}
