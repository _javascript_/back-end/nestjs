import { CreateAccountDto } from 'src/accounts/dto/create-account.dto';

export class CreateUserDto extends CreateAccountDto {}
