import { Injectable } from '@nestjs/common';

import { AccountsService } from 'src/accounts/accounts.service';
import { ProfilesService } from 'src/profiles/profiles.service';
import { RolesService } from 'src/roles/roles.service';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    private accountsService: AccountsService,
    private rolesService: RolesService,
    private profilesService: ProfilesService,
  ) {}

  async create(createUserDto: CreateUserDto) {
    let account = await this.accountsService.create(createUserDto);

    await this.rolesService.addRole(account);
    await this.profilesService.addProfile(account);

    const user = await this.accountsService.findOneById(account.id);

    return user;
  }

  findAll() {
    return [];
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
