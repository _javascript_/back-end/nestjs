import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { ProfilesService } from './profiles.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './entities/profile.entity';

@ApiTags('Профили')
@Controller('profiles')
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) {}

  @ApiOperation({ summary: 'Создание профиля' })
  @ApiResponse({ status: 201, type: Profile })
  @Post()
  create(@Body() createProfileDto: CreateProfileDto) {
    return this.profilesService.create(createProfileDto);
  }

  @ApiOperation({ summary: 'Получиние всех профилей' })
  @ApiResponse({ status: 200, type: [Profile] })
  @Get()
  findAll() {
    return this.profilesService.findAll();
  }

  @ApiOperation({ summary: 'Получиние профиля по ID' })
  @ApiResponse({ status: 200, type: Profile })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.profilesService.findOne(+id);
  }

  @ApiOperation({ summary: 'Изменение профиля по ID' })
  @ApiResponse({ status: 200, type: Profile })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProfileDto: UpdateProfileDto) {
    return this.profilesService.update(+id, updateProfileDto);
  }

  @ApiOperation({ summary: 'Удалиние профиля по ID' })
  @ApiResponse({ status: 200, type: Profile })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.profilesService.remove(+id);
  }
}
