import { ApiProperty } from '@nestjs/swagger';
import { Gender } from '../entities/profile.entity';

export class CreateProfileDto {
  @ApiProperty({ example: 'Иван', description: 'Имя' })
  firstName: string;

  @ApiProperty({ example: 'Иванов', description: 'Фамилия' })
  lastName: string;

  @ApiProperty({ example: 'Иванович', description: 'Отчество' })
  patronymic: string;

  @ApiProperty({ example: '88005553535', description: 'Номер телефона' })
  telephone: string;

  @ApiProperty({
    example: Gender.MALE,
    description: 'Гендорная самоидентификация',
  })
  gender: Gender;
}
