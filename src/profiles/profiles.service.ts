import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from 'src/accounts/entities/account.entity';
import { Repository } from 'typeorm';

import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { Profile } from './entities/profile.entity';

@Injectable()
export class ProfilesService {
  constructor(
    @InjectRepository(Profile) private profilesRepository: Repository<Profile>,
  ) {}

  create(createProfileDto?: CreateProfileDto) {
    const profile = this.profilesRepository.create(createProfileDto);
    return this.profilesRepository.save(profile);
  }

  findAll() {
    return this.profilesRepository.find();
  }

  findOne(id: number) {
    return this.profilesRepository.findOneBy({ id });
  }

  update(id: number, updateProfileDto: UpdateProfileDto) {
    return this.profilesRepository.update(id, updateProfileDto);
  }

  async remove(id: number) {
    const profile = await this.findOne(id);
    return this.profilesRepository.remove(profile);
  }

  async addProfile(account: Account) {
    if (account?.profile?.id) return null;
    const profile = this.profilesRepository.create();
    profile.account = account;
    await this.profilesRepository.save(profile);
    return profile;
  }
}
