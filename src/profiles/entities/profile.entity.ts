import { ApiProperty } from '@nestjs/swagger';
import { Account } from 'src/accounts/entities/account.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
  Other = 'other',
}

@Entity('profiles')
export class Profile {
  @ApiProperty({ example: 1, description: 'Уникальный идентификатор' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'Иван', description: 'Имя' })
  @Column({ default: null })
  firstName: string;

  @ApiProperty({ example: 'Иванов', description: 'Фамилия' })
  @Column({ default: null })
  lastName: string;

  @ApiProperty({ example: 'Иванович', description: 'Отчество' })
  @Column({ default: null })
  patronymic: string;

  @ApiProperty({ example: '88005553535', description: 'Номер телефона' })
  @Column({ default: null })
  telephone: string;

  @ApiProperty({
    example: Gender.MALE,
    description: 'Гендорная самоидентификация',
  })
  @Column({ default: null })
  gender: Gender;

  @OneToOne(() => Account, (account) => account.profile, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  account: Account;
}
