import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Next,
  Post,
  Req,
  Res,
  UsePipes,
} from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';

import { AuthService } from './auth.service';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';

import { ValidationPipe } from 'src/shared/pipes/validation.pipe';
import { NextFunction, Request, Response } from 'express';

const DAY_30 = 30 * 24 * 60 * 1000;

function setRefreshToken(response: Response, token: string) {
  response.cookie('token', token, {
    maxAge: DAY_30,
    httpOnly: true,
    secure: true,
    sameSite: 'none',
  });
}

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Регистрация пользователя' })
  @ApiResponse({ status: 201, type: '' })
  @Post('sign_up')
  @UsePipes(new ValidationPipe())
  async signUp(
    @Body() dto: SignUpDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    const tokens = await this.authService.signUp(dto);
    setRefreshToken(res, tokens.refresh);
    return { accessToken: tokens.access };
  }

  @ApiOperation({ summary: 'Авторизация пользователя' })
  @ApiResponse({ status: 200, type: '' })
  @Post('sign_in')
  @UsePipes(new ValidationPipe())
  async signIn(
    @Body() dto: SignInDto,
    @Res({ passthrough: true }) res: Response,
  ) {
    const tokens = await this.authService.signIn(dto);
    setRefreshToken(res, tokens.refresh);
    return { accessToken: tokens.access };
  }

  @ApiOperation({ summary: 'Выход пользователя' })
  @ApiResponse({ status: 200, type: '' })
  @Get('sign_out')
  async signOut(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    const { token } = req.cookies;
    if (!token) {
      const msg = `Пользователь не авторизован`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
    await this.authService.signOut(token);
    res.clearCookie('token');
    return 'SIGN OUT';
  }

  @ApiOperation({ summary: 'Проверка токенов' })
  @ApiResponse({ status: 200, type: '' })
  @Get('refresh')
  async refresh(
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    const { token } = req.cookies;
    if (!token) {
      const msg = `Пользователь не авторизован`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
    const tokens = await this.authService.refresh(token);
    setRefreshToken(res, tokens.refresh);
    return { accessToken: tokens.access };
  }

  @ApiOperation({ summary: 'Активация аккаунта' })
  @ApiResponse({ status: 200, type: '' })
  @Get('activate')
  activate() {
    return this.authService.activate();
  }
}
