import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { META_KEY_ROLES } from './roles-auth.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>(
      META_KEY_ROLES,
      context.getHandler(),
    );
    console.log('RolesGuard', roles);
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    return matchRoles(roles, user?.roles ?? []);
  }
}

function matchRoles(roles: string[], usersRoles: string[]) {
  return roles.some((role) => usersRoles.some((r) => r === role));
}
