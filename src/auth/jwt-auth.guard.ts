import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { verify } from 'jsonwebtoken';

const accessKey = 'ACCESS_TOKEN_SECRET_KEY';

export class JwtAuthGuard implements CanActivate {
  constructor() {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    if (!req.headers.authorization) {
      const msg = `Пользователь не авторизован`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
    try {
      const [bearer, token] = req.headers.authorization.split(' ');

      if (bearer !== 'Bearer' || !token) {
        const msg = `Пользователь не авторизован`;
        throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
      }

      const candidate = verify(token, accessKey);

      if (!candidate) {
        const msg = `Пользователь не авторизован`;
        throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
      }

      req.user = candidate;

      return true;
    } catch (error) {
      const msg = `Не валидный токен`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
  }
}
