import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AccountsService } from 'src/accounts/accounts.service';
import { ResponseAccountDto } from 'src/accounts/dto/response-account.dto';
import { Account } from 'src/accounts/entities/account.entity';
import { CryptoService } from 'src/crypto/crypto.service';
import { SessionsService } from 'src/sessions/sessions.service';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';

@Injectable()
export class AuthService {
  constructor(
    private accountsService: AccountsService,
    private cryptoService: CryptoService,
    private sessionService: SessionsService,
  ) {}
  async signUp(dto: SignUpDto) {
    const account = await this.accountsService.create(dto);
    return this.getTokens(account);
  }
  async signIn(dto: SignInDto) {
    const { email, password } = dto;

    const candidate = await this.accountsService.findOneByEmail(email);

    if (!candidate) {
      const msg = `Пользователь с почтой "${email}" не найден`;
      throw new HttpException(msg, HttpStatus.NOT_FOUND);
    }

    const isPasswordMatching = await this.cryptoService.compare(
      password,
      candidate.password,
    );

    if (!isPasswordMatching) {
      const msg = `Не верный пароль! (Да-да, так делать нельзя)`;
      throw new HttpException(msg, HttpStatus.BAD_REQUEST);
    }

    return await this.getTokens(candidate);
  }
  async signOut(token: string) {
    const session = await this.sessionService.findOneByToken(token);
    if (!session) {
      const msg = `Пользователь не авторизован`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
    await this.sessionService.remove(session);
  }
  async refresh(token: string) {
    const candidate = this.cryptoService.validateRefreshToken(token) as Account;
    const session = await this.sessionService.findOneByToken(token);
    if (!candidate || !session) {
      const msg = `Пользователь не авторизован`;
      throw new HttpException(msg, HttpStatus.UNAUTHORIZED);
    }
    await this.sessionService.remove(session);

    const account = await this.accountsService.findOneById(candidate.id);
    return this.getTokens(account);
  }
  activate() {
    return 'activate';
  }

  private async getTokens(account: Account) {
    const payload = new ResponseAccountDto(account);
    const tokens = await this.cryptoService.generateJwtTokens(
      payload.toObject(),
    );

    await this.sessionService.create(account, {
      token: tokens.refresh,
      ip: 'test-test',
      data: JSON.stringify(payload.toObject()),
    });

    return tokens;
  }
}
