import { SetMetadata } from '@nestjs/common';
import { RoleValue } from 'src/roles/entities/role.entity';

export const META_KEY_ROLES = 'metakey_roles';

export const Roles = (...roles: RoleValue[]) =>
  SetMetadata(META_KEY_ROLES, roles);
