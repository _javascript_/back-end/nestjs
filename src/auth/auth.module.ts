import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { AccountsModule } from 'src/accounts/accounts.module';
import { CryptoModule } from 'src/crypto/crypto.module';
import { SessionsModule } from 'src/sessions/sessions.module';

@Module({
  imports: [forwardRef(() => AccountsModule), CryptoModule, SessionsModule],
  controllers: [AuthController],
  providers: [AuthService],
  exports: [AuthModule],
})
export class AuthModule {}
