import { Injectable } from '@nestjs/common';
import { hash, compare } from 'bcrypt';
import { sign, verify } from 'jsonwebtoken';

@Injectable()
export class CryptoService {
  private accessKey = 'ACCESS_TOKEN_SECRET_KEY';
  private refreshKey = 'REFRESH_TOKEN_SECRET_KEY';

  async hash(value: string) {
    return await hash(value, 3);
  }

  async compare(data: string | Buffer, encrypted: string) {
    return await compare(data, encrypted);
  }

  async generateJwtTokens(payload: object | Buffer) {
    const access = sign(payload, this.accessKey, { expiresIn: '10m' });
    const refresh = sign(payload, this.refreshKey, { expiresIn: '30d' });
    return { access, refresh };
  }

  validateRefreshToken(token: string) {
    return verify(token, this.refreshKey);
  }

  validateAccessToken(token: string) {
    return verify(token, this.accessKey);
  }
}
