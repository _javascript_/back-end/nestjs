export type ResponseStatus =
  | 'information'
  | 'success'
  | 'redirect'
  | 'client-error'
  | 'server-error';

const STATUS_MESSAGE: { [key: number]: string } = {
  200: 'OK',
  201: 'Create',
  202: 'Accepted',
  203: 'Non-Authoritative Information',
  204: 'No Content',
  400: 'Bad Request',
  401: 'Unauthorized',
  403: 'Forbidden',
  404: 'Not Found',
  500: 'Internal Server Error',
};

export class ResponsePacket {
  status: ResponseStatus;
  code: number;
  message: string;

  data?: any;
  errors?: any;

  constructor(code: number, message?: string, data?: any) {
    const isErrorsData = code >= 400;
    if (isErrorsData) this.errors = data.errors;
    else this.data = data;
    this.code = code;
    this.message = message ? message : this.getMessage(code);
    this.status = this.getStatus(code);
  }

  private getStatus(code: number) {
    if (code >= 100 && code < 200) return 'information';
    if (code >= 200 && code < 300) return 'success';
    if (code >= 300 && code < 400) return 'redirect';
    if (code >= 400 && code < 500) return 'client-error';
    return 'server-error';
  }

  private getMessage(code: number) {
    return STATUS_MESSAGE[code] ?? this.getStatus(code).toUpperCase();
  }
}
