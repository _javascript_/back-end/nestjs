import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponsePacket } from '../utils/response';

export interface Response<T> {
  data: T;
}

@Injectable()
export class TransformDataInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const now = new Date();
    return next.handle().pipe(
      map((data) => {
        const response = context.switchToHttp().getResponse();
        const { statusCode, message } = response;
        const packet = new ResponsePacket(statusCode, message, data);
        return {
          ...packet,
          time: {
            start: now.toISOString(),
            end: new Date().toISOString(),
            ms: Date.now() - now.getTime(),
          },
        };
      }),
    );
  }
}
