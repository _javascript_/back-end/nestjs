import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { ResponsePacket } from '../utils/response';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  private logger = new Logger('HTTP-ERROR');

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    const packet = new ResponsePacket(
      status,
      exception.message,
      exception.getResponse(),
    );

    this.logger.error(`${packet.message}`);

    response.status(status).json({
      ...packet,
      path: request.url,
    });
  }
}
